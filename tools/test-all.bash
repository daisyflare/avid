#!/usr/bin/env bash

set -xe

./tools/error-tests.bash
cargo test --workspace
cargo doc -q --lib --no-deps
