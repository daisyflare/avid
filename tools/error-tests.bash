#!/usr/bin/env bash

# TODO(#30): Rewrite this in Avid

bad=""

case "$(basename "$(pwd)")" in
    "tools")
        cd ../src
        ;;
    "avid")
      cd src
      ;;
esac

grep -rn "\.*todo!(" | grep -v "\S*:\S*:\s*///" > tmp
if [ -f tmp ]; then
    if [ "$(cat tmp)" ]; then
        perl -e "use Term::ANSIColor;" -e "print color('bold red');" -e "print \"These have 'todo!()'\n\n\";" -e "print color('reset');"
        cat tmp
        bad="yes"
    fi
fi

grep -rn ".*Err(" | grep -v -e "\S*:\S*:\s*//" -e "\S*tests.rs" > tmp

grep -v ".* // test:.*$" tmp > tmp2
if [ -f tmp2 ]; then
    if [ "$(cat tmp2)" ]; then
        perl -e "use Term::ANSIColor;" -e "print color('bold red');" -e "print \"\n\nThese don't have associated tests!\n\n\";" -e "print color('reset');"
        cat tmp2
        bad="yes"
    fi
fi

for c in $(grep ".* // test:.*$" tmp | sed "s/.* \/\/ test:\(.*\)$/\1/"); do
    if [ ! "$(grep "fn $c()" tests.rs)" ]; then
            grep "test:$c\s*$" tmp >> tmp3
    fi
done

if [ -f tmp3 ]; then
    if [ "$(cat tmp3)" ]; then
        perl -e "use Term::ANSIColor;" -e "print color('bold red');" -e "print \"\n\nThese connect to tests that don't exist!\n\n\";" -e "print color('reset');"
        cat tmp3
        bad="yes"
    fi
fi

rm -f tmp tmp2 tmp3

if [ $bad ]; then
    exit 1
fi
