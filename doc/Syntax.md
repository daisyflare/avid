# Avid Syntax
Avid's syntax is heavily inspired by forth and uses [reverse polish notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation).
This means that all functions operate on an implicit "stack," pushing and popping
elements as needed. Let's take a look at an example:
```ignore
/* stack is now empty */

/* Pushes `1` to the stack */
1

/* stack: 1 */
/* Pushes `2` to the stack */
2

/* stack: 1 2 */
/* Pops the top two elements from the stack, sums them up, and pushes the result onto the stack */
+

/* stack: 3 */
/* Pops an element from the top of the stack and prints it. */
print

/* stack is now empty */
```

Avid also allows strings:
```ignore
/* Stack is now empty */
/* Pushes itself onto the stack */
"Hello, "

/* Stack: "Hello, " */
"World!"

/* Stack: "Hello, " "World!" */
/* Swaps the top two items on the stack */
swap

/* Stack: "World!" "Hello, " */
/* Prints "Hello, World!" */
print print 


/* This prints: */
/*
all of these escape codes are allowed!: "Quotes", preventing new lines, adding
 new lines, 'Single quotes,'
urns,age ret
         tabs, and \\ slashes! \\
*\
"all of these escape codes are allowed!: \"Quotes\", preventing \
new lines, adding \n new lines, \'Single quotes,\' 
carriage ret\rurns,
\t tabs, and \\ slashes! \\" print


/* Stack is now empty */
```

And lists:
```ignore
/* Stack is now empty */
/* Pushes a new list to the stack */
list

/* Stack: [] */
/* Pushes several new items to the stack and appends each to the list */
1 append
2 append
3 append

/* Stack: [1, 2, 3] */
/* Copies the top item on the stack */
dup

/* Stack: [1, 2, 3] [1, 2, 3] */
/* Prints: [1, 2, 3] */

/* Stack: [1, 2, 3] */
/* The list can be used again */
```

## Control Flow
Control flow is very simple in Avid: an `if` statement just pops from the top of the stack, sees
if that value is truthy, and continues into the `if` statement if it is. `while` loops are similar:
on each iteration of the loop, the code between the `while` and the `do` parts is executed, and
if what is left on top of the stack is truthy, the code inside the block is executed and
execution returns to the `while` for reevaluation.

Both of these types of blocks are terminated by `end` statements.

Now, some examples!

```ignore
0
while
    1 +
    dup 11 != dup

    /* You can do if statements anywhere, even inside the conditions of while loops! */
    if
        "The value is not eleven! " print
    end
do
    "The value is: " print
    dup print
    "! " print
end
```

## Functions
Currently, you cannot define your own functions in Avid, but you can in rust! Check out the rust documentation for more details, and [the list of pre-defined functions](./StandardLib.md) too!
