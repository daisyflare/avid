# The Avid Standard Library
Avid's standard library is relatively small and simple. So, without further ado, let's get into it!

## Basic functions
| Function | Description                                                                                                                                                                                                                                 |
|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `+`      | Pops the top two items off of the stack, sums them up, and pushes the result onto the stack. Throws an error if the types at the top are not `Num`, `Num`.                                                                                  |
| `-`      | Pops the top two items off of the stack, finds their difference, and pushes the result onto the stack. Throws an error if the types at the top are not `Num`, `Num`.                                                                        |
| `dup`    | Makes a copy of the top element of the stack and pushes it onto the stack.                                                                                                                                                                  |
| `swap`   | Swaps the top two elements on the stack.                                                                                                                                                                                                    |
| `drop`   | Removes the top element on the stack.                                                                                                                                                                                                       |
| `list`   | Creates a new list and pushes it onto the stack.                                                                                                                                                                                            |
| `append` | Pops the value from the top of the stack and appends it to the second element on the stack. Throws an error if the second element on the stack is not a `List`.                                                                             |
| `not`    | Pops the top element on the stack, performs a logical not, and pushes the result onto the stack.                                                                                                                                            |
| `==`     | Pops the top two elements on the stack, checks their equality, and pushes the result onto the stack.                                                                                                                                        |
| `!=`     | Pops the top two elements on the stack, checks their inequality, and pushes the result onto the stack.                                                                                                                                      |
| `rot`    | Pops the top three elements from the top of the stack (`arg3`, `arg2`, `arg1`), then pushes the first element, then the third, then the second onto the stack (`arg1`, `arg3`, `arg2`).                                                     |

## I/O functions
These are all of the I/O (input-output) functions that Avid supports. Note that these functions may be disabled for various reasons, so check with the program that you are using!
| Function | Description |
|:--|:--|
| `print` | Pops the top element from the stack and prints it to standard output |

