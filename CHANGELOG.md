# Changelog

All notable changes to Avid are documented in this file.
The sections should follow the order `Packaging`, `Added`, `Changed`, `Fixed` and `Removed`.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## 0.7.1 - Tue, Mar 21, 2023

Happy new year! 🥳 🎉 (I'm only a little late, right?)

### Added 
 - Coloured error representation.
 - Show origin of errors in error messages
 - Togglable `colour` feature to use colour while printing errors
   - New method for `Error`: `force_colour`, and associated documentation 
 - `Note` type to give suggestions for easily fixed errors
 - Internal `cli_format!` macro to simplify writing output for errors, etc

### Changed
 - Interpreter moved into its own package
 - `Error` type is considerably smaller
 
### Fixed 
 - No longer crashes upon runtime error on even-numbered lines. 

## 0.7.0 - Thu, Dec 29, 2022

### Added
 - Reading and writing indexes
 - New internal type: `RawIndex`
 - New soon-to-be-public type: `Index`
   - This hides implementation details and will be better for indexing when custom types are introduced
 - New errors for new functionality: `RWIndexWithoutAccess`, `MisformedIndex`, `InvalidIndex`, `InvalidProperty`, `CantBeIndexed`, `IndexNotFound`
 - Test-tracking tool
 
### Changed
 - *Type now implements `Display`

### Fixed
 - Stack-debugging tool produces clearer result
 
### Removed
 - Builtin function: `get`

## 0.6.1 - Sat, Oct 8, 2022

### Changed
 - Now licensed under LGPL as opposed to GPL
 - Parser now uses `Iterator` trait
 - Interpreter now shows the name of the file where the error occurred

## 0.6.0 - Thu, Sep 15, 2022

### Added
 - New method for `Builder`: `new_ast`, and associated documentation

### Changed
 - Syntax documentation is now available on the front page of the documentation
 - `Builder` type can now build both `Ast` and `Avid` structs
 - Update relevant documentation for `Ast`, `Avid`, `Builder`

## 0.5.1 - Tue, Aug 30, 2022

### Added
 - Location of errors in error messages
 - Documentation for `AstBuilder` and its method: `src_name`
 - Documentation for `Builder` and its method: `src_name`
 - Documentation for `Location`
 - Documentation for `Error`
 - New method for `AstBuilder`: `promise`, and associated documentation

## 0.5.0 - Tue, Jul 26, 2022

### Added
 - `Ast` type
 - `AstBuilder` type
 - Documentation for `Ast` and its methods: `new`
 - Documentation for `AstBuilder` and its methods: `compile`, `register_fn`, `new`, `remove`
 - New methods for `Avid`: `new`, `from_ast`, `from_ast_mut`, and associated documentation
 - New method for `Builder`: `new_ast`, and associated documentation
 - Tests for new functionality

### Changed
 - `AvidBuilder` -> `Builder`

## 0.4.0 - Fri, Jun 24, 2022

### Added 
 - `Promises` type
 - `PromiseBuilder` type
 - Documentation for `Promises` and its method: `count`
 - Documentation for `PromiseBuilder` and its methods: `build`, `new`, `add_promise`
 - Documentation for `AvidBuilder::promise()`

### Fixed 
 - Tests with new API
 - Bug where "0" would not be recognised as an integer.

## 0.3.1 - Fri, Jun 24, 2022

### Added
 - Negative numbers
 - Different base numbers: 2, 8, 16
 - Multi-line strings
 - Escaped characters in strings
 - License
 - Contributing guidelines

## 0.3.0 - Thu, Jun 23, 2022

### Added
 - Boolean type and "truthiness"
 - Elseless if-statements
 - While loops
 - Errors on malformed or unclosed if or while statements
 - More basic operations: `drop`, `swap`, `dup`, `list`, `append`, `not`, `==`, `!=`, `get`, `rot`
 - Comments
 - More tests for added functionality
 - Language docs (vs library docs)
 - Fibonacci example

### Changed
 - The interpreter now accepts paths to files containing source code, rather than source code itself

## 0.2.1 - Sun, Jun 19, 2022

### Added
 - Front page documentation
 - Documentation for `Avid` and its methods: `run()`, `run_mut()`
 - Documentation for `AvidBuilder` and its methods: `build()`, `new()`, `removed()`, `no_io()`
 - Documentation for `Stack` and its methods: `new()`, `into_objects()`, `push()`, `pop()`, `pop_typed()`
 - Doctests for above documentation
 - Documentation for `AvidErr` and its variants
 - Documentation for `ParseError`
 - Documentation for `Object`

### Fixed
 - Compilation errors in interpreter and tests

## 0.2.0 - Fri, Jun 17, 2022

### Added
 - AvidBuilder API to customise configuration of the Avid struct.

### Fixed
 - Bug in parsing that produced errors depending upon the order of the items in source code.

## 0.1.0 - Wed, Jun 15, 2022

### Added
 - Functionality to run a small snippet of code
 - Tests for new functionality

### Fixed
 - Behaviour where error messages show one line and column before they actually should be.

## pre-0.1.0 - Tue, Jun 14, 2022

### Added 
 - Built-in operations: `print`, `+`, `-`, push int to stack, push str to stack
 - Errors: `NotEnoughArgs`, `IncorrectType`
 - Functionality to use all rust functions with matching signatures in the program
