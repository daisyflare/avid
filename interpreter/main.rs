use std::{env, fs, process::{self, exit}};

use avid::Builder;

fn main() {
    let mut args = env::args();
    let name = args.next().unwrap();

    let filepath = args.next().map(|x| {
        (
            fs::read_to_string(&x).expect("Could not read from file!"),
            x,
        )
    });

    if let Some((src, fname)) = filepath {
        #[allow(unused_mut)]
        if let Err(mut e) = run(src, fname) {
            if atty::is(atty::Stream::Stderr) {
                println!("{}", e.force_colour());
                const BOLD_WHITE: &str = "\x1b[1;37m";
                const RESET: &str = "\x1b[m";
                eprintln!("{BOLD_WHITE}{}{RESET}: Exiting because an error occurred.", e.loc);
            } else {
                eprintln!("{e}\n{}: Exiting because an error occurred.", e.loc);
            }
            exit(1);
        }
    } else {
        eprintln!("Usage: {name} <code to be interpreted>");
        process::exit(1);
    }
}

fn run(src: String, fname: String) -> avid::Result<()> {
    let avid = Builder::new(&src)
        .allow_stack_debug(true)
        .src_name(fname)
        .build()?;
    avid.run(None)?;
    Ok(())
}
