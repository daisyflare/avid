# Avid
A configuration language for programs with a Forth-like syntax, intended to be as plug-and-play as possible.

## NOTE: The language is currently in a beta state
I am working on fixing this as soon as possible, but it should be known!

## Documentation
To see the Avid documentation, check out [the syntax](./doc/Syntax.md) and [the standard library](./doc/StandardLib.md)!

## Quick Start
To add it as a configuration language to your program, add it to your dependencies in your Cargo.toml, like so:
``` toml
[dependencies]
avid = {version = "*", git = "https://www.gitlab.com/daisyflare/avid"}
```

If you want to just use the language itself, you can do that like so:

``` shell
$ git clone "https://www.gitlab.com/daisyflare/avid"
$ cd avid
$ cargo build --release && cp target/release/avid .
$ ./avid
```
