# Contributing
Submit a merge request on the [gitlab](https://www.gitlab.com/daisyflare/avid) to add a contribution.

## IMPORTANT
All pull requests *must* compile, pass all tests, and have documentation for new/changed public APIs. Also, changes should be summarised in CHANGELOG.md. **I WILL NOT ACCEPT PRS IF THEY DO NOT COMPLY WITH THIS!**

For ease of convenience, if [this tester](./tools/test-all.bash) runs successfully, you should be fine.

