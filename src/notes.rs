use crate::{Error, errors::DEFAULT_WIDTH, cli_format};

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Note {
    Similar(String),
}

impl Note {
    pub fn fmt_with_context(&self, f: &mut std::fmt::Formatter<'_>, err: &Error) -> std::fmt::Result {

        #[cfg(feature = "colour")]
        let use_colour = err.coloured;
        #[cfg(not(feature = "colour"))]
        let use_colour = false;

        let message = match self {
            Self::Similar(s) => format!("a similar item exists: `{s}`.\n")
        };
        cli_format!(NOTE: f, &err.loc, use_colour, message)?;
        match self {
            Self::Similar(s) => {
                let (before, after) = err.original.split_at(err.loc.col - 1);
                let len = s.len();
                let (_, uncoloured) = after.split_at(err.loc.len);
                write!(f, "{uncoloured}")?;
                cli_format! {
                    SUGGESTION: f, &err.loc, use_colour, "try replacing it" =>
"\\
{before}<BRIGHT_GREEN>{s}<RESET>\0
<BRIGHT_GREEN>{:>offset$}{:~<len$}<RESET>
" => '^', '~', offset = err.loc.col, len = len.saturating_sub(1)
                }
            }
        }
    }
}

pub(crate) struct Levenshteiner {
    // Vectors are stored as a mild optimization
    v0: Vec<usize>,
    v1: Vec<usize>,
    to_compare: Vec<char>,
    max_distance: Option<usize>,
    best_so_far: Option<(Vec<char>, usize)>
}

impl Levenshteiner {
    pub(crate) fn new<D: Into<Option<usize>>>(to_compare: &str, max_distance: D) -> Self {
        Self {
            v0: Vec::with_capacity(to_compare.len()),
            v1: Vec::with_capacity(to_compare.len()),
            to_compare: to_compare.chars().collect(),
            max_distance: max_distance.into(),
            best_so_far: None,
        }
    }

    // Levenshtein algorithm taken from:
    // https://en.wikipedia.org/wiki/Levenshtein_distance#Iterative_with_two_matrix_rows
    pub(crate) fn add(&mut self, to_test: &str) {
        let to_test = to_test.chars().collect::<Vec<_>>();
        if to_test.len() >= self.v0.len() {
            let discrepancy = to_test.len() - self.v0.len() + 1;
            self.v0.extend(vec![0; discrepancy]);
            self.v1.extend(vec![0; discrepancy]);
        }
        assert!(to_test.len() < self.v0.len(), "{}, {}", to_test.len(), self.v0.len());
        for i in 0..=to_test.len() {
            self.v0[i] = i
        }
        for i in 0..self.to_compare.len() {
            self.v1[0] = i + 1;

            #[allow(clippy::needless_range_loop)]
            for j in 0..to_test.len() {
                let del_cost = self.v0[j + 1] + 1;
                let ins_cost = self.v1[j] + 1;
                let sub_cost = if self.to_compare[i] == to_test[j] {
                    self.v0[j]
                } else {
                    self.v0[j] + 1
                };
                self.v1[j + 1] = del_cost.min(ins_cost).min(sub_cost);
            }
            std::mem::swap(&mut self.v0, &mut self.v1);
        }
        let dist = self.v0[to_test.len()];
        if dist <= self.max_distance.unwrap_or(usize::MAX) && self.best_so_far.as_ref().map(|(_, len)| dist < *len).unwrap_or(true) {
            self.best_so_far = Some((to_test, dist));
        }
    }

    pub(crate) fn finish(self) -> Option<String> {
        self.best_so_far.map(|(s, _)| s.into_iter().collect())
    }
}
