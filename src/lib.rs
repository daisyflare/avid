//! Plug and play configuration language.
//!
//! Provides a simple API to insert this configuration language into your project. The
//! language is easy to learn and checks for many common errors, including unknown
//! variables and malformed statements, at (Avid's) compile time.
//!
//! The available APIs and other settings can be tweaked or modified easily by using the
//! different methods on the [Builder] struct.
//!
//! # Examples
//! ```
//! use avid::{Builder, Stack};
//!
//! let src = "1 2 + print \"Hello, World!\" print-to-log";
//! let mut log = String::new();
//!
//! let avid = Builder::new(src)
//!     // Add a new function that pops one item from the top of the stack
//!     // and prints it to the log
//!     .register_fn("print-to-log", |stack: &mut Stack| {
//!         let [to_print] = stack.pop()?;
//!         log.push_str(&to_print.to_string());
//!         Ok(())
//!     })
//!     .build().unwrap();
//!
//! // Prints `3` to standard output
//! avid.run(None).unwrap();
//!
//! assert_eq!(&log, "Hello, World!");
//! ```
//!
//! You can use any function with the correct signature, including closures.
//!
#![doc = include_str!("./../doc/Syntax.md")]
//!
// TODO(#27): Fix "unparseable code" error when including stdlib docs
#![doc = include_str!("./../doc/StandardLib.md")]
#![allow(rustdoc::invalid_rust_codeblocks)]

#![warn(rustdoc::all)]
#![warn(rustdoc::missing_doc_code_examples)]
#![warn(missing_docs)]

#[macro_use]
mod macros;
mod ast;
mod boo;
mod builder;
mod errors;
mod func;
mod parser;
mod promises;
mod notes;
mod stack;
#[cfg(test)]
mod tests;
mod typing;

pub use ast::Ast;
pub use builder::Builder;
pub use errors::{Result, Error, ErrorKind, Location};
pub use promises::{PromiseBuilder, Promises};
pub use stack::{Avid, Stack};
pub use typing::{Object, ObjectType};
