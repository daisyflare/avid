use std::{fmt, cmp::Ordering};

use crate::ErrorKind;

type VecObj = Vec<Object>;

#[derive(Debug, PartialEq, Clone)]
pub enum Index<'a> {
    Ident(&'a str),
    Object(&'a Object)
}

impl<'a> Index<'a> {
    #[inline(always)]
    pub fn unwrap_object(&self) -> &'a Object {
        match self {
            Index::Ident(i) => panic!("Could not unwrap Ident value: {i:?}!"),
            Index::Object(o) => o,
        }
    }
}

enum_type! {
    /// The possible types in an Avid program.
    Object {
        /// A number.
        Num(isize),
        /// A string
        String(String),
        /// A boolean
        Bool(bool),
        /// A list of objects
        List(VecObj)
    }
}

impl Clone for Object {
    fn clone(&self) -> Self {
        match self {
            Self::Num(i) => Self::Num(*i),
            Self::String(s) => Self::String(s.clone()),
            Self::Bool(b) => Self::Bool(*b),
            Self::List(l) => Self::List(l.clone()),
        }
    }
}

impl Object {
    /// Whether the object evaluates to "true" in a condition.
    ///
    /// For now, the only instances that evaluate to `false` are `false`, `""`,
    /// `0`, and the empty list.
    pub fn truthy(&self) -> bool {
        let cond = match self {
            Object::Num(n) => *n == 0,
            Object::String(s) => s.as_str() == "",
            Object::Bool(b) => !*b,
            Object::List(l) => l.is_empty(),
        };
        !cond
    }

    // Index-get, called by `value : field @`
    pub(crate) fn read_index(&self, idx: &Index) -> Result<Object, ErrorKind> {
        use Object::*;
        match self {
            Num(_) | String(_) | Bool(_) => Err(ErrorKind::CantBeIndexed(self.get_type())), // test:unindexable_types
            List(l) => {
                match idx {
                    Index::Ident(i) => {
                        if *i == "len" {
                            Ok(Object::Num(l.len() as isize))
                        } else {
                            Err(ErrorKind::InvalidProperty { invalid: i.to_string(), valid: Some(vec!["len".to_string()]) }) // test:invalid_list_property
                        }
                    },
                    Index::Object(Object::Bool(_) | Object::String(_) | Object::List(_)) => Err(ErrorKind::InvalidIndex { attempted_indexed: ObjectType::List, provided_type: idx.unwrap_object().get_type(), possible_types: Some(vec![ObjectType::Num]) }), // test:invalid_index_type
                    Index::Object(Object::Num(n)) => {
                        if let Ok(n) = usize::try_from(*n) {
                            match l.get(n) {
                                Some(s) => Ok(s.clone()),
                                None => Err(ErrorKind::IndexNotFound), // test:index_not_found
                            }
                        } else {
                            Err(ErrorKind::IndexNotFound) // test:index_not_found
                        }
                    },
                }
            },
        }
    }

    // Index-set, called by `value : field !`
    pub(crate) fn write_index(&mut self, idx: &Index, value: Object) -> Result<(), ErrorKind> {
        use Object::*;
        match self {
            Num(_) | String(_) | Bool(_) => Err(ErrorKind::CantBeIndexed(self.get_type())), // test:unindexable_types
            List(l) => {
                match idx {
                    Index::Ident(i) => Err(ErrorKind::InvalidProperty { invalid: i.to_string(), valid: None }), // test:invalid_list_property
                    Index::Object(Object::Bool(_) | Object::String(_) | Object::List(_)) => Err(ErrorKind::InvalidIndex { attempted_indexed: ObjectType::List, provided_type: idx.unwrap_object().get_type(), possible_types: Some(vec![ObjectType::Num]) }), // test:invalid_index_type
                    Index::Object(Object::Num(n)) => {
                        if let Ok(n) = usize::try_from(*n) {
                            match n.cmp(&l.len()) {
                                Ordering::Less => l[n] = value,
                                Ordering::Equal => l.push(value),
                                Ordering::Greater => return Err(ErrorKind::IndexNotFound), // test:index_not_found
                            }
                            Ok(())
                        } else {
                            Err(ErrorKind::IndexNotFound) // test:index_not_found
                        }
                    },
                }
            },
        }
    }
}

impl fmt::Display for Object {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Num(n) => write!(f, "{n}"),
            Self::String(s) => write!(f, "{s}"),
            Self::Bool(b) => write!(f, "{b}"),
            Self::List(l) => {
                write!(f, "[")?;
                for (idx, obj) in l.iter().enumerate() {
                    if idx > 0 {
                        write!(f, ", ")?;
                    }
                    write!(f, "{obj}")?;
                }
                write!(f, "]")
            }
        }
    }
}
