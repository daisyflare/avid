use std::{fmt::{Display, self}, sync};

use crate::{ObjectType, notes::{Note, Levenshteiner}, parser::Parser, builder};

// These are so the docs link correctly.
#[allow(unused_imports)]
use crate::{Builder, Stack, PromiseBuilder, Promises};

/// A shorthand for a Result type with Avid programs.
///
/// This is more for convenience than anything else, so it can be pretty safely ignored.
pub type Result<T, E = Error> = std::result::Result<T, E>;

/// An error in an Avid program, along with the location it occurred.
///
/// This implements [Display](std::fmt::Display) and so error messages can be reported
/// with macros like [println].
///
/// # Examples
/// Accessing the error's type and location:
/// ```
/// # let source_code = "";
/// use avid::{Avid, ErrorKind};
///
/// if let Err(e) = Avid::new(source_code) {
///     eprintln!("There was an error at {}!", e.loc);
///     if let ErrorKind::UnclosedIf = *e.kind {
///         eprintln!("You forgot to close your if statement!");
///     } else {
///         eprintln!("All if statements are good.")
///     }
/// }
/// ```
///
/// Showing error messages:
/// ```
/// use avid::Avid;
///
/// let src = "5 \"hi\" +";
/// let avid = Avid::new(src).unwrap();
///
/// if let Err(e) = avid.run(None) {
///     eprintln!("{}", e);
/// }
/// /* Standard error now reads:
///
///    f.avid:1:8: Error: Incorrect Argument Type: expected [Num, Num], got [Num, String]!
///    1 | 5 "hi" +
///      |        ^
///
/// */
/// ```
#[derive(Debug, PartialEq)]
pub struct Error {
    /// The type of error that was thrown.
    pub kind: Box<ErrorKind>,
    /// The location where the error was thrown.
    pub loc: Location,
    // The original line of code
    pub(crate) original: String,
    // Whether or not to render with ANSI colour codes
    #[cfg(feature = "colour")]
    pub(crate) coloured: bool,
    pub(crate) notes: Vec<Note>,
}

impl Error {
    #[inline(always)]
    pub(crate) fn new_with_notes(kind: ErrorKind, loc: Location, source: &str, notes: Vec<Note>) -> Self {
        let original = source.lines().nth(loc.line - 1).expect("Location should always hold a valid line!");
        Self {
            kind: Box::new(kind),
            loc,
            original: original.to_string(),
            #[cfg(feature = "colour")]
            coloured: false,
            notes
        }
    }

    #[inline(always)]
    pub(crate) fn new(kind: ErrorKind, loc: Location, source: &str) -> Self {
        // TODO(#22): Add more Notes
        // - Possibility for missing `rot` and `dup`
        if let ErrorKind::UnknownVar { name: _ } = kind {
            panic!("UnknownVar variant should be handled by Error::new_not_found!")
        }
        Self::new_with_notes(kind, loc, source, Vec::new())
    }

    pub(crate) fn new_not_found<'b, T: builder::BuildTarget<'b, 'b>>(name: String, loc: Location, parser: &Parser<'_, 'b, T>) -> Self {
        const MAX_DISTANCE: usize = 4;
        let mut l = Levenshteiner::new(&name, MAX_DISTANCE);
        for name in parser.names() {
            l.add(name);
        }
        let mut notes = Vec::new();
        if let Some(closest) = l.finish() {
            notes.push(Note::Similar(closest));
        }
        Self::new_with_notes(ErrorKind::UnknownVar { name }, loc, parser.source(), notes)
    }

    /// Force the error to be printed with colour as defined by the ANSI terminal spec.
    ///
    /// # Note
    /// If the error is printed into something other than a tty (a string, a file, etc),
    /// there will be 'garbage' in it that represents the colour to the terminal, so make
    /// sure where exactly the output is going before using this function.
    ///
    /// # Example
    /// ```ignore
    /// let err = erroring_function();
    /// if atty::is(atty::Stream::Stderr) {
    ///     println!("{}\nExiting!", e.force_colour());
    /// } else {
    ///     eprintln!("{e}\nExiting!");
    /// }
    /// ```
    #[cfg(feature = "colour")]
    pub fn force_colour(&mut self) -> &mut Self {
        self.coloured = true;
        self
    }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
#[allow(dead_code)]
pub(crate) enum ColourType {
    Reset = 0,
    BrightRed,
    BrightWhite,
    BrightPink,
    BrightGreen,
    None,
    Count
}

impl From<ColourType> for &'static str {
    fn from(val: ColourType) -> Self {
        match val {
            ColourType::Reset => "\x1b[m",
            ColourType::BrightRed => "\x1b[1;91m",
            ColourType::BrightWhite => "\x1b[1;37m",
            ColourType::BrightPink => "\x1b[1;35m",
            ColourType::BrightGreen => "\x1b[1;92m",
            ColourType::None => "",
            ColourType::Count => unreachable!(),
        }
    }
}

impl fmt::Display for ColourType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s: &str = (*self).into();
        write!(f, "{s}")
    }
}

#[derive(Clone, Debug)]
pub(crate) struct ColourParser<'a> {
    maps: Vec<(&'a str, ColourType)>
}

static_assert_eq!(ColourType::Count as usize - 1, 5);

impl<'a> ColourParser<'a> {
    #[allow(unused)]
    pub(crate) fn add_map(&mut self, to: &'a str, from: ColourType) {
        self.maps.push((to, from));
    }

    pub(crate) fn basic(coloured: bool) -> Self {
        use ColourType::*;
        let cols = if coloured {
            vec![("<RESET>", Reset), ("<BRIGHT_RED>", BrightRed), ("<BRIGHT_WHITE>", BrightWhite), ("<BRIGHT_PINK>", BrightPink), ("<BRIGHT_GREEN>", BrightGreen)]
        } else {
            vec![("<RESET>", None), ("<BRIGHT_RED>", None), ("<BRIGHT_WHITE>", None), ("<BRIGHT_PINK>", None), ("<BRIGHT_GREEN>", None)]
        };
        Self {
            maps: cols,
        }
    }

    // NOTE: This can probably be optimised easily to build only one new string rather than a new one for each entry in self.maps
    pub(crate) fn map(&self, s: &str) -> String {
        let mut to_return: Option<String> = None;
        for (pat, repl) in &self.maps {
            let s = to_return.as_ref().map_or(s, String::as_str);
            to_return = Some(s.replace(pat, (*repl).into()));
        }
        to_return.expect("There should be at least one entry in self.maps!")
    }
}

// TODO(#23): Add tests for cli_format!()
#[macro_export]
#[doc(hidden)]
macro_rules! cli_format {
    (ERROR: $f:expr, $loc:expr, $coloured:expr, $short_msg:expr => $fmt:expr => $($arg:tt)*) => {
        cli_format!(BASIC: "error", "BRIGHT_RED" => $f, $loc, $coloured, $short_msg => $fmt => $($arg)*)
    };
    (NOTE: $f:expr, $loc:expr, $coloured:expr, $short_msg:expr) => {
        cli_format!(BASIC: "note", "BRIGHT_PINK" => $f, $loc, $coloured, $short_msg => "" => )
    };
    (NOTE: $f:expr, $loc:expr, $coloured:expr, $short_msg:expr => $fmt:expr => $($arg:tt)*) => {
        cli_format!(BASIC: "note", "BRIGHT_PINK" => $f, $loc, $coloured, $short_msg => $fmt => $($arg)*)
    };
    (SUGGESTION: $f:expr, $loc:expr, $coloured:expr, $short_msg:expr => $fmt:expr => $($arg:tt)*) => {
        cli_format!(BASIC: "suggestion", "BRIGHT_GREEN" => $f, $loc, $coloured, $short_msg => $fmt => $($arg)*)
    };
    (BASIC: $typ:expr, $colour:expr => $f:expr, $loc:expr, $coloured:expr, $short_msg:expr => $fmt:expr => $($arg:tt)*) => {
        {
            #[allow(unused_imports)]
            use std::fmt::Write;
            let mut msg = format!($fmt, $($arg)*);
            let line_prefix = format!("\n{:>DEFAULT_WIDTH$} | ", $loc.line);
            let empty_prefix = format!("\n{:>DEFAULT_WIDTH$} | ", ' ');
            msg = msg.trim_start().replace("\\\n", &line_prefix).replace("\0\n", &empty_prefix);
            let replacer = $crate::errors::ColourParser::basic($coloured);
            let loc = $loc;
            let short_msg = $short_msg;
            let s = format!("<BRIGHT_WHITE>{loc}:<RESET> <{}>{}:<RESET> {short_msg}{m}", $colour, $typ, m = msg.trim_end());
            let to_print = replacer.map(s.as_str());
            write!($f, "{to_print}")
        }
    };
}

pub(crate) const DEFAULT_WIDTH: usize = 5;
impl Display for Error {
    #[allow(non_snake_case)]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        #[cfg(feature = "colour")]
        let use_colour = self.coloured;
        #[cfg(not(feature = "colour"))]
        let use_colour = false;

        // TODO(#24): Non-ascii codepoints may cause errors in Error::display

        // TODO(#25): Logical error in Error::display
        // logical errors on multiline strings will occur because the math assumes one line
        let (before, after) = self.original.split_at(self.loc.col - 1);

        // NOTE: .split_at should never panic because col + len *should* be less than after.len()
        // However, this might(?) be broken on multiline errors
        let (coloured, uncoloured) = after.split_at(self.loc.len);

        cli_format!{
            ERROR: f, &self.loc, use_colour, &self.kind =>
"\\
{before}<BRIGHT_RED>{coloured}<RESET>{uncoloured}\0
<BRIGHT_RED>{:>offset$}{:~<len$}<RESET>
" => '^', '~', offset = self.loc.col, len = self.loc.len.saturating_sub(1)
        }?;


        for note in &self.notes {
            writeln!(f)?;
            note.fmt_with_context(f, self)?;
        }
        Ok(())
    }
}

/// A location in a file.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Location {
    /// The line where the error occurred.
    pub line: usize,
    /// The column where the error occurred.
    pub col: usize,
    /// The file in which the error occurred.
    ///
    /// This file is sometimes not provided if the method [Builder::src_name] is not used. In
    /// this case, `file_name` is `None`.
    // TODO(#14): Make Location.file_name use &str instead of Arc<String>.
    // This should be possible if the ownership of the file names is moved into the
    // Avid instance/Ast.
    pub file_name: Option<sync::Arc<String>>,
    // The length of the token
    pub(crate) len: usize,
}

impl Display for Location {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{}:{}", self.file_name.as_ref().map_or("<provided>", |a| a.as_str()), self.line, self.col)
    }
}

impl Display for ErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ErrorKind::NotEnoughArgs { expected, got } => write!(f, "Not Enough Arguments: expected {expected}, got {got}!"),
            ErrorKind::IncorrectType { expected, got } => write!(f, "Incorrect Argument Type: expected {expected:?}, got {got:?}!"),
            ErrorKind::UnknownVar { name } => write!(f, "Unknown Variable: `{name}`!"),
            ErrorKind::UnfulfilledPromise { expected } => write!(f, "Unfulfilled Promise: `{expected}`!\nNote: This is ususally the fault of the program interpreting the code. You should file a bug report if possible."),
            ErrorKind::UnpairedEnd => write!(f, "`End` Without Start of Block!"),
            ErrorKind::UnclosedIf => write!(f, "`if` Statement Without Matching `end`!"),
            ErrorKind::UnclosedWhile => write!(f, "`while` Statement Without Matching `end`!"),
            ErrorKind::WhileWithoutDo => write!(f, "`while` Statement Without `do`!"),
            ErrorKind::DoWithoutWhile => write!(f, "Unexpected `do`!"),
            ErrorKind::UnclosedString => write!(f, "Unclosed String!"),
            ErrorKind::IncorrectNumber => write!(f, "Unparseable Number!"),
            ErrorKind::UnknownEscape => write!(f, "Unknown Escaped Character!"),
            ErrorKind::UnclosedIndexer => write!(f, "`:` without matching `!` or `@`!"),
            ErrorKind::RWIndexWithoutAccess => write!(f, "`!` or `@` without previous `:`!"),
            ErrorKind::MisformedIndex => write!(f, "Misformed Index!"),
            ErrorKind::InvalidIndex { attempted_indexed, provided_type, possible_types } => {
                if let Some(possible_types) = possible_types {
                    write!(f, "Invalid Index: {attempted_indexed} cannot be indexed by {provided_type}!\nNote: It can be indexed by {possible_types:?}")
                } else {
                    write!(f, "Invalid Index: {attempted_indexed} cannot be indexed by {provided_type}!")
                }
            },
            ErrorKind::IndexNotFound => write!(f, "Index not found!"),
            ErrorKind::InvalidProperty { invalid, valid } => {
                if let Some(valid) = valid {
                    write!(f, "{invalid:?} is an invalid property for the operation! Valid propert{}: {valid:?}", if valid.len() == 1 { "y" } else { "ies" })
                } else {
                    write!(f, "{invalid:?} is an invalid property for the operation! There are no valid properties!")
                }
            },
            ErrorKind::CantBeIndexed(t) => write!(f, "{t:?} cannot be indexed!"),
        }
    }
}

/// Errors that can happen when running an Avid program.
///
/// These errors are caused by the authors of the Avid code and should be caught.
/// When writing APIs with [register_fn](Builder::register_fn()), it is expected that you return the
/// correct kind of error.
#[derive(Debug, PartialEq)]
// TODO(#15): Separate out errors into compile-time errors and runtime errors.
// Maybe also collect the compile time errors into a Vec<CompileError> or something.
pub enum ErrorKind {
    /// When not enough arguments are present on the stack for an operation.
    ///
    /// This error is automatically returned by [pop](Stack::pop) and [pop_typed](Stack::pop_typed) when
    /// there are insufficient arguments.
    NotEnoughArgs {
        /// The number of items expected on the stack.
        expected: usize,
        /// The number of items actually on the stack.
        got: usize
    },
    /// When the arguments on the stack are the incorrect kind for the operation.
    IncorrectType {
        /// The types expected on the stack.
        ///
        /// Currently, they must be requested statically, but they will be able to be requested
        /// dynamically in a later update.
        // TODO(#16): Make ErrorKind::IncorrectType.expected use Vec<ObjectType> instead of &'static [ObjectType].
        //
        // This will make it much easier for functions that might have dynamic signatures.
        expected: &'static [ObjectType],
        /// The types actually found on the stack.
        got: Vec<ObjectType>,
    },
    /// When the user tries to use a variable or function that does not exist.
    UnknownVar {
        /// The name of the unknown variable or function.
        name: String
    },
    /// When promises do not get fulfilled.
    ///
    /// Promises can be made with [Builder::promise] and fulfilled by passing them into
    /// a [Promises] struct with [PromiseBuilder::add_promise].
    UnfulfilledPromise {
        /// The promise that was made in [Builder::build] but not found at runtime.
        expected: String
    },
    /// An unpaired `end`, which is expected to finish a statement but there is no statement found.
    UnpairedEnd,
    /// An unclosed `if` statement, such as `true if print`.
    UnclosedIf,
    /// An unclosed `while` statement, such as `true while do print`.
    UnclosedWhile,
    /// A while loop without the `do`. For example: `while true end`.
    WhileWithoutDo,
    /// A `do ... end` statement without a starting `while`.
    DoWithoutWhile,
    /// An unclosed string. The location in the source code will point to the end of
    /// the data of the string, right before where closing quotes should be.
    UnclosedString,
    /// An unparseable number. The location in the source code will point to the start
    /// of where the number should be.
    IncorrectNumber,
    /// An invalid escape code. The current valid escapes in Avid are: `\n`, `\\n`
    /// (cancels out a new line in a string), `\t`, `\\`, `\"`, `\'`, and `\r`.
    UnknownEscape,
    /// When an indexer (`:`) is not followed by a read (`@`) or write (`!`) token
    /// within two tokens, such as `some_struct : field other_field 5 !`. This
    /// error is also emitted when there is no closing operation to an indexer
    /// at all: `some_struct :` will generate an error.
    UnclosedIndexer,
    /// A read or write operation (`@` or `@`) without a preceding index access (`:`)
    RWIndexWithoutAccess,
    /// An invalid index value that does not raise other errors, such as in
    /// `some_struct true if : end @`.
    MisformedIndex,
    /// When the indexing operation fails because the index is an invalid type, such as
    /// in `list : "hi" @`.
    InvalidIndex {
        /// The object that was attempted to be indexed. In the above example, it would be
        /// `List`.
        attempted_indexed: ObjectType,
        /// The type of the index that didn't work. In the above example, it would be `String`.
        provided_type: ObjectType,
        /// The possible types that the object can be indexed with. In the above example, it would
        /// be `Some([Number])`
        possible_types: Option<Vec<ObjectType>>
    },
    /// When the indexing operation fails because the item does not have the specified property.
    /// For example, `list : abcd @` will return `InvalidProperty { "abcd", Some(["len"]) }`.
    InvalidProperty {
        /// The property that could not be accessed.
        invalid: String,
        /// The valid properties that the object has.
        valid: Option<Vec<String>>
    },
    /// When the indexing operation fails because the item that would be indexed cannot
    /// be indexed, such as in `5 : 1 @`. The value is the type of the item that was attempted to
    /// be indexed. In the above example, it would be `Number`.
    CantBeIndexed(ObjectType),
    /// When the indexing operation fails because the index is not present,
    /// such as in `list : 5 @`.
    // TODO(#26): provide the index that was not found
    IndexNotFound
}
