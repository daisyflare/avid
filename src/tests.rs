#![cfg(test)]
use std::collections::{HashMap, HashSet};

use crate::{
    func::{AvidFunc, Builtin},
    parser::{Lexer, Parser, TokenData, NUM_ESCAPES},
    stack::Stack,
    typing::{Object, ObjectType},
    Avid, Builder, ErrorKind, Ast, PromiseBuilder,
};

fn call_ops<T: AvidFunc>(ops: &mut [T]) -> crate::Result<Stack, ErrorKind> {
    let mut stack = Stack::new();

    for op in ops {
        op.call(&mut stack)?;
    }
    Ok(stack)
}

#[test]
fn add() {
    let mut ops = [Builtin::PushInt(34), Builtin::PushInt(35), Builtin::Sum];

    let mut stack = call_ops(&mut ops).unwrap();

    assert_eq!(Object::Num(69), stack.pop::<1>().unwrap()[0])
}

#[test]
fn subtract() {
    let mut ops = [
        Builtin::PushInt(34),
        Builtin::PushInt(35),
        Builtin::Subtract,
    ];

    let mut stack = call_ops(&mut ops).unwrap();

    assert_eq!(Object::Num(-1), stack.pop::<1>().unwrap()[0])
}

#[test]
fn not_enough_on_stack() {
    let mut ops = [Builtin::Print];

    let res = call_ops(&mut ops);

    assert_eq!(
        res,
        Err(ErrorKind::NotEnoughArgs {
            expected: 1,
            got: 0
        })
    )
}

#[test]
fn incorrect_types() {
    let mut ops = [
        Builtin::PushInt(5),
        Builtin::PushStr("kfdj".to_string()),
        Builtin::Sum,
    ];

    let res = call_ops(&mut ops);

    assert_eq!(
        res,
        Err(ErrorKind::IncorrectType {
            expected: &[ObjectType::Num, ObjectType::Num],
            got: vec![ObjectType::Num, ObjectType::String]
        })
    )
}

#[test]
fn parses_correctly() {
    let lexer = Lexer::new("1 2 3 - + -fgh -22 \"Hello, World!\" print", None);

    let toks = lexer.map(|tok| tok.unwrap().data).collect::<Vec<_>>();

    let expected = vec![
        TokenData::Int(1),
        TokenData::Int(2),
        TokenData::Int(3),
        TokenData::Promise("-".to_string()),
        TokenData::Promise("+".to_string()),
        TokenData::Promise("-fgh".to_string()),
        TokenData::Int(-22),
        TokenData::String("Hello, World!".to_string()),
        TokenData::Promise("print".to_string()),
    ];

    assert_eq!(toks, expected)
}

#[test]
fn handles_unfinished_escape() {
    let mut lexer = Lexer::new("\"\\\"", None);
    let tok = lexer.next().unwrap().map_err(|e| e.kind);
    assert_eq!(
        tok,
        Err(Box::new(ErrorKind::UnclosedString))
    );
}

#[test]
fn handles_unfinished_string() {
    let mut lexer = Lexer::new("\"hi", None);
    let tok = lexer.next().unwrap().map_err(|e| e.kind);

    assert_eq!(
        tok,
        Err(Box::new(ErrorKind::UnclosedString))
    );
}

#[test]
fn handles_incorrect_number() {
    let srcs = ["2a", "-99b", "-9a92", "228jief"];
    for src in srcs {
        let mut lexer = Lexer::new(src, None);
        let tok = lexer.next().unwrap().map_err(|x| x.kind);

        assert_eq!(tok, Err(Box::new(ErrorKind::IncorrectNumber)))
    }
}

#[test]
fn handles_incorrect_prefixed_number() {
    for c in '\0'..='\u{10FFFF}' {
        let allowed = ['x', 'b', 'o', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '\n', '\t'];
        if allowed.contains(&c) {
            continue;
        }
        let src = format!("0{c}012");
        let mut l = Lexer::new(&src, None);
        let t = l.next().unwrap().map_err(|e| e.kind);
        assert_eq!(t, Err(Box::new(ErrorKind::IncorrectNumber)), "Character {c} ({c:?}) should not be allowed in a number!");
    }
}

#[test]
fn handles_unclosed_indexer() {
    let res = Builder::new(":").build().unwrap_err().kind;
    assert_eq!(res, Box::new(ErrorKind::UnclosedIndexer));
}

#[test]
fn runs_correctly() {
    let src = "32 35 + 2 +";

    let avid = Avid::new(src).unwrap();

    let mut stack = avid.run(None).unwrap();

    assert_eq!(stack.pop::<1>(), Ok([Object::Num(69)]))
}

#[test]
fn pop_untyped() {
    let mut stack = Stack::new();

    stack.push(Object::Num(1));
    stack.push(Object::Num(2));

    let res = stack.pop::<2>();

    assert_eq!(res, Ok([Object::Num(1), Object::Num(2)]));

    let res = stack.pop::<5>();

    assert_eq!(
        res,
        Err(ErrorKind::NotEnoughArgs {
            expected: 5,
            got: 0
        })
    );
}

#[test]
fn pop_typed() {
    let mut stack = Stack::new();

    stack.push(Object::String("sus".to_string()));
    stack.push(Object::Num(2));

    const EXPECTED_TYPES: [ObjectType; 2] = [ObjectType::Num, ObjectType::Num];

    let res = stack.pop_typed(&EXPECTED_TYPES);

    assert_eq!(
        res,
        Err(ErrorKind::IncorrectType {
            expected: &EXPECTED_TYPES,
            got: vec![ObjectType::String, ObjectType::Num]
        })
    );

    let res = stack.pop::<5>();

    assert_eq!(
        res,
        Err(ErrorKind::NotEnoughArgs {
            expected: 5,
            got: 2
        })
    );
}

#[test]
fn lexer_unclosed_string() {
    let mut lexer = Lexer::new("\"Hi", None);

    let res = lexer.next().map(|x| x.map_err(|e| e.kind));

    assert_eq!(res, Some(Err(Box::new(ErrorKind::UnclosedString))))
}

#[test]
fn lexer_string() {
    let mut lexer = Lexer::new("\"Hi\"", None);

    let res = lexer.next().map(|x| x.map(|t| t.data));

    assert_eq!(res, Some(Ok(TokenData::String("Hi".to_string()))))
}

#[test]
fn lexer_number() {
    let lexer = Lexer::new("420 -69 0b101 0o11 0xF6 0", None);

    let res: Vec<TokenData> = lexer.map(|r| r.unwrap().data).collect();

    let expected: Vec<TokenData> = vec![420, -69, 5, 9, 246, 0]
        .into_iter()
        .map(TokenData::Int)
        .collect();

    assert_eq!(res, expected);
}

#[test]
fn lexer_bad_number() {
    let mut lexer = Lexer::new("5zgy", None);

    let res = lexer.next().unwrap().unwrap_err().kind;

    assert_eq!(*res, ErrorKind::IncorrectNumber);
}

#[test]
fn handles_unknown_var() {
    let src = "amogus";

    let provided = HashMap::new();

    let parser = Parser::<Ast>::new(src, None, HashSet::new(), provided);

    let res = parser.parse().map_err(|e| e.kind).unwrap_err();

    assert_eq!(
        *res,
        ErrorKind::UnknownVar {
            name: "amogus".to_string()
        }
    )
}

#[test]
fn handles_double_indexer() {
    let res = Builder::new(": :").build().unwrap_err().kind;

    assert_eq!(*res, ErrorKind::UnclosedIndexer);
}

#[test]
fn rw_without_access() {
    let res = Builder::new("@").build().unwrap_err().kind;

    assert_eq!(*res, ErrorKind::RWIndexWithoutAccess);

    let res = Builder::new("!").build().unwrap_err().kind;

    assert_eq!(*res, ErrorKind::RWIndexWithoutAccess);
}

#[test]
fn handles_unmatched_end() {
    let src = "end";

    let res = Avid::new(src).map_err(|e| e.kind);

    assert_eq!(res, Err(Box::new(ErrorKind::UnpairedEnd)))
}

#[test]
fn handles_basic_if() {
    let src = "1 true if 2 end false if 3 end";

    let avid = Avid::new(src).unwrap();

    let stack = avid.run(None).unwrap().into_objects();

    assert_eq!(stack, vec![Object::Num(1), Object::Num(2)])
}

#[test]
fn unclosed_if() {
    let src = "true if";

    let res = Avid::new(src).map_err(|e| e.kind);

    assert_eq!(res, Err(Box::new(ErrorKind::UnclosedIf)));
}

#[test]
fn unclosed_while() {
    let res = Builder::new("while").build().unwrap_err().kind;

    assert_eq!(*res, ErrorKind::UnclosedWhile);

    let res = Builder::new("while do").build().unwrap_err().kind;

    assert_eq!(*res, ErrorKind::UnclosedWhile);
}

#[test]
fn do_without_while() {
    let res = Builder::new("if do end").build().unwrap_err().kind;

    assert_eq!(*res, ErrorKind::DoWithoutWhile);

    let res = Builder::new("do end").build().unwrap_err().kind;

    assert_eq!(*res, ErrorKind::DoWithoutWhile);
}

#[test]
fn while_without_do() {
    let res = Builder::new("while end").build().unwrap_err().kind;

    assert_eq!(*res, ErrorKind::WhileWithoutDo);
}

#[test]
fn unpaired_end() {
    let res = Builder::new("end").build().unwrap_err().kind;

    assert_eq!(*res, ErrorKind::UnpairedEnd);
}

#[test]
fn unindexable_types() {
    let res = Avid::new("false : 2 @").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::CantBeIndexed(ObjectType::Bool));

    let res = Avid::new("2 : 2 @").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::CantBeIndexed(ObjectType::Num));

    // TODO(#29): String indexing
    let res = Avid::new("\"j\" : 2 @").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::CantBeIndexed(ObjectType::String));

    let res = Avid::new("false 1 : 2 !").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::CantBeIndexed(ObjectType::Bool));

    let res = Avid::new("2 1 : 2 !").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::CantBeIndexed(ObjectType::Num));

    let res = Avid::new("\"j\" 1 : 2 !").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::CantBeIndexed(ObjectType::String));
}

#[test]
fn invalid_list_property() {
    let res = Avid::new("list : j @").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::InvalidProperty { invalid: "j".to_string(), valid: Some(vec!["len".to_string()]) });

    let res = Avid::new("list 1 : len !").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::InvalidProperty { invalid: "len".to_string(), valid: None })
}

#[test]
fn invalid_index_type() {
    let res = Avid::new("list : \"\" @").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::InvalidIndex { attempted_indexed: ObjectType::List, provided_type: ObjectType::String, possible_types: Some(vec![ObjectType::Num]) });

    let res = Avid::new("list : false @").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::InvalidIndex { attempted_indexed: ObjectType::List, provided_type: ObjectType::Bool, possible_types: Some(vec![ObjectType::Num]) });

    let res = Avid::new("list 1 : \"\" !").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::InvalidIndex { attempted_indexed: ObjectType::List, provided_type: ObjectType::String, possible_types: Some(vec![ObjectType::Num]) });

    let res = Avid::new("list 1 : false !").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::InvalidIndex { attempted_indexed: ObjectType::List, provided_type: ObjectType::Bool, possible_types: Some(vec![ObjectType::Num]) });
}

#[test]
fn index_not_found() {
    let res = Avid::new("list : -1 @").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::IndexNotFound);

    let res = Avid::new("list : 77 @").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::IndexNotFound);

    let res = Avid::new("list 7 : -1 !").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::IndexNotFound);

    let res = Avid::new("list 7 : 77 !").unwrap().run(None).unwrap_err().kind;
    assert_eq!(*res, ErrorKind::IndexNotFound);
}

#[test]
fn indexing() {
    let res: Vec<isize> = Avid::new("list 5 : 0 ! 3 : 1 !").unwrap().run(None).unwrap().into_objects().remove(0).unwrap_list().into_iter().map(|x| x.unwrap_num()).collect();
    assert_eq!(res, vec![5, 3]);

    let res: Vec<isize> = Avid::new("list 5 : 0 ! 3 : 1 ! 1 : 0 !").unwrap().run(None).unwrap().into_objects().remove(0).unwrap_list().into_iter().map(|x| x.unwrap_num()).collect();
    assert_eq!(res, vec![1, 3]);
}

#[test]
fn indexer_too_far_away() {
    let res = Builder::new("list : len 1 9 @").build().unwrap_err().kind;

    assert_eq!(*res, ErrorKind::UnclosedIndexer);
}

#[test]
fn parse_into_ast() {
    let src = "push-a print";

    let a = "Hello from the other thread!!".to_string();

    let ast = Builder::new_ast(src)
        .register_fn("push-a", |s: &mut Stack| {
            let b = &a;
            s.push(Object::String(b.clone()));
            Ok(())
        })
        .build()
        .unwrap();

    Avid::from_ast(ast).run(None).unwrap();
}

// This is intentionally empty. It's to simplify error-checking tools.
#[test]
fn ignore() {}

#[test]
fn escaped_characters() {
    let src = "\"\\n \\t \\\\ \\\" \\' \\r\"";

    let mut l = Lexer::new(src, None);

    let tok = l.next().unwrap().unwrap();

    assert_eq!(tok.data.unwrap_string(), "\n \t \\ \" ' \r");

    static_assert_eq!(NUM_ESCAPES, 7, "Change number of listed escapes!");
    for c in '\0'..='\u{10FFFF}' {
        let allowed = ['n', '\n', 't', '\\', '"', '\'', 'r'];
        if allowed.contains(&c) {
            continue;
        }
        let src = format!("\"\\{c}\"");
        let mut l = Lexer::new(&src, None);
        let tok = l.next().unwrap().map_err(|e| e.kind);
        assert_eq!(tok, Err(Box::new(ErrorKind::UnknownEscape)), "Char {c} (c:?) should not be an allowable escape value!")
    }
}

#[test]
fn unfulfilled_promise() {
    let res = Builder::new("unknown")
        .promise("unknown")
        .build()
        .unwrap();

    let r = res.run(None).unwrap_err().kind;

    assert_eq!(r, Box::new(ErrorKind::UnfulfilledPromise { expected: "unknown".to_string() }));

    let res = Builder::new("unknown")
        .promise("unknown")
        .build()
        .unwrap();

    let mut promises = PromiseBuilder::new().build();

    let r = res.run(Some(&mut promises)).unwrap_err().kind;

    assert_eq!(r, Box::new(ErrorKind::UnfulfilledPromise { expected: "unknown".to_string() }))
}

#[test]
fn read_index_stack_empty() {
    let res = Avid::new(": 5 @").unwrap().run(None).unwrap_err().kind;

    assert_eq!(*res, ErrorKind::NotEnoughArgs { expected: 1, got: 0 });

    let res = Avid::new(": @").unwrap().run(None).unwrap_err().kind;

    assert_eq!(*res, ErrorKind::NotEnoughArgs { expected: 2, got: 0 });
}

#[test]
fn write_index_stack_empty() {
    let res = Avid::new(": 5 !").unwrap().run(None).unwrap_err().kind;

    assert_eq!(*res, ErrorKind::NotEnoughArgs { expected: 2, got: 0 });

    let res = Avid::new(": !").unwrap().run(None).unwrap_err().kind;

    assert_eq!(*res, ErrorKind::NotEnoughArgs { expected: 3, got: 0 });
}

#[test]
fn test_error_reporting() {
    let src = [
        "+",
        "1 \n\"b\" +",
        "\n\n\njb",
        "unknown",
        "end",
        "if",
        "while\n do\n",
        "while",
        "do",
        "\"",
        "8fjdd",
        "\\j"
    ];

    fn map_fn(line: &str) -> crate::Result<()> {
        Builder::new(line)
            .src_name("example")
            .promise("unknown")
            .build()?
            .run(None)?;
        Ok(())
    }

    let _ = src
        .into_iter()
        .map(|line| {
            format!("{}", map_fn(line).unwrap_err())
        })
        .collect::<Vec<_>>();
}

#[test]
fn misformed_indexes() {
    let srcs = [
        ": if @",
        ": while @ do",
        "while : do @",
        "if : end @",
        "while do : end @",
    ];

    for src in srcs {
        let err = Avid::new(src).unwrap_err().kind;
        assert_eq!(*err, ErrorKind::MisformedIndex);
    }
}
